use master;

-- Kills all active connections to database before dropping and creating database.
WHILE EXISTS(select NULL from sys.databases where name='dhsa')
    BEGIN
        DECLARE @SQL varchar(max)
        SELECT @SQL = COALESCE(@SQL,'') + 'Kill ' + Convert(varchar, SPId) + ';'
        FROM MASTER..SysProcesses
        WHERE DBId = DB_ID(N'dhsa') AND SPId <> @@SPId
        EXEC(@SQL)
        DROP DATABASE [dhsa]
    END
GO

CREATE DATABASE dhsa
    COLLATE SQL_Latin1_General_CP1_CI_AS;
GO
--drop database if exists dhsa;
--go
--create database dhsa;
--go
use dhsa;
go

-- Create user dhsa with membership to db_datareader and db_datawriter
CREATE USER [dhsa] FOR LOGIN [dhsa] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE db_datareader ADD MEMBER dhsa
GO
ALTER ROLE db_datawriter ADD MEMBER dhsa
GO

create table codesystem (
    codesystem_oid nvarchar(60) not null primary key,
    codesystem_json nvarchar(4000) constraint
    [codesystem_json record should be formatted as JSON] check (ISJSON(codesystem_json)=1)
);
go

-- unikt = code_id + parent_oid
create table code (
    code_id nvarchar(60) not null,
    parent_oid nvarchar(60) not null foreign key references codesystem(codesystem_oid) on delete cascade,
    CONSTRAINT PK_code PRIMARY KEY (code_id, parent_oid),
    code_json nvarchar(4000) constraint
    [code_json record should be formatted as JSON] check (ISJSON(code_json)=1)
);
go
