Lokal installation av SQL Server 2019 i Docker.

1. Gå till 'hsa-codesystem/codesystem-war'

2. Kör 'docker-compose up' (skriptet terminerar inte)

3. Anslut till databasen på localhost:1443 med föredraget verktyg.

4. Kör script för att initiera databasen : dbsetup.sql

5. Kör script för att populera databasen : dbfill.sql