#!/bin/bash
# copy files
HSA_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." >/dev/null 2>&1 && pwd )"

echo $HSA_ROOT
shopt -s nocasematch
WINPTY_CMD=""
case "`uname`" in
	darwin*)  WINPTY_CMD="" ;;
	linux*)   WINPTY_CMD="" ;;
	mingw*)   WINPTY_CMD="winpty" ;;
	msys*)    WINPTY_CMD="" ;;
	*)        echo "unknown: `uname`" ;;
esac



# codesystems data
echo "Copying codesystem files to hsa_sqlserver"
docker cp "${HSA_ROOT}/hsa/data/sqlserver/codesystems_sql/dbsetup.sql" hsa-sqlserver:/
docker cp "${HSA_ROOT}/hsa/data/sqlserver/codesystems_sql/dbfill.sql" hsa-sqlserver:/


# import
echo "Importing codesystem data to hsa_sqlserver"
sleep 30
${WINPTY_CMD} docker exec -it hsa-sqlserver sh -c "/opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P 'YourNewStrong!Passw0rd' < /dbsetup.sql"
${WINPTY_CMD} docker exec -it hsa-sqlserver sh -c "/opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P 'YourNewStrong!Passw0rd' < /dbfill.sql"


# organization data
# TODO