# 1. Git Commit Best Practices

Date: 2018-05-24

## Status

Accepted

## Context

We need a concise and consistent approach to commiting code.  Developers should always strive for making intuitive commit messages and make the the progress tracable in other parts of the projects.

## Decision

We will rely on best practices and recommendations from GitHub and other higly respected players.

## Consequences

Follow advice listed in the following articles:
https://github.com/trein/dev-best-practices/wiki/Git-Commit-Best-Practices
https://chris.beams.io/posts/git-commit/
