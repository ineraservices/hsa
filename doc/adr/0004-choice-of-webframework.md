# 4.  Choice of web framework

Date: 2018-10-05

## Status

Evaluation of Vue.js

## Context

We have decided on Vue.js because it seems easiest to work with and we believe it will exist in 3 years.

The choice was a hard one, and we did not always agree.  The final list of alternatives were:

- Angular
- React
- Vue
- Polymer

We will re-evaluate the framework decision in the future.

## Decision

A prototype was succesfully created with Vue.  The next step will be to move on to "Anonymous search".

## Consequences

**Benefits of Vue.js:**

- Empowered HTML.  This means that Vue.js has many similar characteristics with Angular and this can help to optimize HTML blocks handling with a usage of different components.
- Detailed documentation.  Vue.js has very circumstantial documentation which can fasten learning curve for developers and save a lot of time to develop an app using only the basic knowledge of HTML and JavaScript.
- Adaptability.  It provides a rapid switching period from other frameworks to Vue.js because of the similarity with Angular and React in terms of design and architecture.
- Awesome integration.  Vue.js can be used for both building single-page applications and more difficult web interfaces of apps.  The main thing is that smaller interactive parts can be easily integrated into the existing infrastructure with no negative effect on the entire system.
- Large scaling.  Vue.js can help to develop pretty large reusable templates that can be made with no extra time allocated for that according to its simple structure.
- Tiny size.  Vue.js can weight around 20KB keeping its speed and flexibility that allows reaching much better performance in comparison to other frameworks.

**Drawbacks of Vue.js:**

- Lack of resources.  Vue.js still has a pretty small market share in comparison with React or Angular, which means that knowledge sharing in this framework is still in the beginning phase.
- Risk of over flexibility.  Sometimes, Vue.js might have issues while integrating into huge projects and there is still no experience with possible solutions, but they will definitely come soon.
- Chinese background.  As far as Vue.js has a bit of Chinese background, a lot of elements and descriptions are still available in Chinese.  This leads to a partial complexity on some stages of development, nevertheless, more and more materials are being translated into English.

Source: <https://www.linkedin.com/pulse/reactjs-vs-angular5-vuejs-what-choose-2018-c-a-l-i-c-u-t-/>
