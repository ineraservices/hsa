# 3. use java 8

Date: 2018-10-04

## Status

Accepted

## Context

Which java version will the project support?

## Decision

We have chosen java 8 for now. Reasons:

- Everyone has Java 8, last minor version, installed
- We have issues with Apache DS on all versions other than Java 8
- However, we should switch to Java 11 soon, Java 8 is EOL

## Consequences
