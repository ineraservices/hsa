# 5.  Choice of web test tool

Date: 2018-10-26

## Status

Accepted

## Context

The contenders are Cypress and TestCafé, both modern frameworks under active development. They both contain free open source frameworks as well as optional commercial test tools. An evaluation was made of the tools and below follows a summary:

#### TestCafé
##### Great stuff

* Can run in any browser (not reliant on specific drivers), and therefore more portable and self-sustained.
* Can run on remote devices, for example mobile phones
* 3rd party Gherkin support, planned official support
* TestCafé Live, watches test files and automatically reruns tests and stops at the end (does not work with gherkin)

##### Good stuff

* Debugging in chrome dev tools or VS Code.
* Component selectors for Vue

##### Not so good stuff

* No TypeScript support when used with the existing Gherkin package.
* Runs in Node, access to browser stuff like the DOM must be serialized. I’ve yet to discover any limitations because of this however.

#### Cypress
##### Great stuff

* Can manipulate state, and skip tedious UI operations to get to a starting point.
* Runs in the browser (has direct access to every aspect of your application)
* Can record videos of test runs
* Nice electron app for running and debugging tests.
* 3rd party Gherkin support

##### Good stuff

* Full TypeScript support
* jQuery-selectors

##### Not so good stuff

* Can only run in browsers with a supporting driver, might be more hassle getting this to work oustide of chrome.
* Cannot run on remote devices

## Decision

TestCafé will be used together with the gherkin-testcafe package that adds a test runner for Gherkin.

## Consequences

**Benefits of TestCafé:**
* The browser support is a killer feature for us. It enables us to reach our largest target IE11 with ease.
* The future for Gherkin-support is bright.

**Drawbacks of TestCafé:**
* We lose some of the nice to have developer experience-features provided by Cypress. 
* The package Gherkin hides some of the features and functionality provided by TestCafé and is dependant on a third party developer. However since official Gherkin-support is planned 


Source: <https://medium.com/yld-engineering-blog/evaluating-cypress-and-testcafe-for-end-to-end-testing-fcd0303d2103/>


