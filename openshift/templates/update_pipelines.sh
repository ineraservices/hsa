#!/bin/bash
# This will update all the pipelines. If these commands change due to changes in the template
# this file must be updated
oc process -f pipelinetemplate-service.yaml APP_NAME=hsa-codesystem TESTS=st TARGET_PORT=9080 TARGET_URL=http://hsa-codesystem | oc replace -f -
oc process -f pipelinetemplate-service.yaml APP_NAME=hsa-organization TESTS=st TARGET_PORT=9080 TARGET_URL=http://hsa-organization | oc replace -f -
oc process -f pipelinetemplate-service.yaml APP_NAME=hsa-ldap-proxy TESTS=st,cucumber TARGET_PORT=1389 TARGET_URL=hsa-ldap-proxy | oc replace -f -
oc process -f pipelinetemplate-service.yaml APP_NAME=hsa-web TESTS=testcafe-headless TARGET_PORT=8080 TARGET_URL=http://hsa-web | oc replace -f -
