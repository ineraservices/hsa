# Using templates to create resources in OpenShift.

Templates are documents that when processed with parameters outputs one or more resources in OpenShift. This is so we can easily recreate everything on openshift through CLI in a consistent and deterministic way. 
For the example commands below, if you create the resource for the first time - use _oc create_. To update a resource, use _oc replace_ instead.

### buildtemplate-service.yaml

This template will output four resources:

* A Build Configuration for an -artifact image, that will contain the output from a gradle build of a service.
* A Build Configuration for the service-image, which is the runtime image (i.e. hsa-openliberty-rest) combined with the built artifact.
* An Image Stream for each of the Build Configuration where the built images will reside. These can be used as input for other builds or deployments.

| Parameter     | Required | Description                   |
| -----------   | -------- | ----------------------------- |
| BUILD_NAME    | Yes      | The name of the build, i.e. hsa-search | 
| APP_NAME      | Yes      | The name of the app this build is a part of, i.e. hsa-web |
| BASE_IMAGE    | Yes      | The image used as a container for the running application |
| GIT_URL       | Yes      | The https URL to the repo |
| GIT_CONTEXT   | No       | The path within the git repo where the source code resides. Default is '/' |
| ARTIFACT_NAME | Yes      | The output artifact of the build (backend = *.war | frontend=dist/)|
| BUILDER_IMAGE | Yes      | The S2I-image that should be used for building |
| HAS_TESTS     | No       | 'true' or 'false' depending on if this build contains tests that should be run in the pipeline. Default is 'true' |

Usage example:
```
oc process -f buildtemplate-service.yaml GIT_URL=https://bitbucket.org/ineraservices/hsa-organization.git BUILD_NAME=hsa-organization APP_NAME=hsa-organization BASE_IMAGE=hsa-openliberty-rest ARTIFACT_NAME=*.war BUILDER_IMAGE=hsa-s2i-backend | oc create -f -
```

### deploytemplate-service.yaml

This template will output a deployment configuration. With a service-image as input, this resource will specify how this image should be deployed, i.e. resource allocation, port openings, configuration maps to be used etc.

| Parameter          | Required | Description                                        |
| ------------------ | -------- | -------------------------------------------------- |
| APP_NAME           | Yes      | The name, i.e. hsa-ldap-proxy                      |
| IMAGE_NAME         | Yes      | The image that will be deployed                    |
| OPENLIBERTY_CONFIG | Yes      | ConfigMap containing bootstrap.xml for Openliberty |
| PORT               | Yes      | Port to be exposed for the service 9080 (http), 1389 (LDAP) |

Usage example:

```
oc process -f deploytemplate-service.yaml APP_NAME=hsa-organization IMAGE_NAME=hsa-organization OPENLIBERTY_CONFIG=organization-bootstrap PORT=9080 | oc create -f -
```

### deploytemplate-webapp.yaml

This template works like deploytemplate-service.yaml but outputs a deployment configuration with two containers - one frontend and one corresponding service (backend).

| Parameter          | Required | Description                                        |
| ------------------ | -------- | -------------------------------------------------- |
| APP_NAME           | Yes      | The name, i.e. hsa-ldap-proxy                      |
| FRONTEND_IMAGE     | Yes      | The frontend image                                 |
| SERVICE_IMAGE      | Yes      | The service image                                  |

Usage example:

```
oc process -f deploytemplate-webapp.yaml APP_NAME=hsa-web FRONTEND_IMAGE=hsa-search SERVICE_IMAGE=hsa-graphql | oc create -f -
```


### pipelinetemplate-service.yaml

This template will setup a buildconfig with a Jenkins pipeline that will build, deploy and test a service.If you create a pipeline for the first time, you also need to create a webhook that will be used to trigger it. Use the same APP\_NAME.

There is a helper script used to update all the pipelines when changes in the template have been made. (update_pipelines.sh). Remember to update this script if the parameters below change.

| Parameter   | Required | Description                   |
| ----------- | -------- | ----------------------------- |
| APP_NAME    | Yes      | The name, i.e. hsa-ldap-proxy |
| TARGET_URL  | Yes      | The url to the application under test |
| TARGET_PORT | Yes      | The port to the application under test |
| TESTS       | Yes      | A comma separated list of build tasks that should be run to test the application |

Usage example:

```
oc process -f pipelinetemplate-service.yaml APP_NAME=hsa-organization TESTS=st,cucumber TARGET_PORT=9080 TARGET_URL=http://hsa-organization| oc create -f -
```

### webhooktemplate.yaml

This template will create a webhook that can be used to trigger a build from bitbucket. Since the link to the generated webhook will be protected by VPN, the link in bitbucket will target a reverse proxy at NMT wit the following format:

https://webhook2.nordicmedtest.se/dhsa/<pipeline>/<secret>

*pipeline* is the exact name of the buildconfig that is the pipeline.
*secret* is the webhook secret, and is the result of running this template.

| Parameter   | Required | Description                   |
| ----------- | -------- | ----------------------------- |
| APP_NAME    | Yes      | The name, i.e. hsa-ldap-proxy |

Usage example:

```
oc process -f webhooktemplate.yaml APP_NAME=hsa-organization | oc create -f -
```

### buildtemplate-docker.yaml

This template will create a docker build to turn a Dockerfile into an image in an ImageStream on openshift. It will also create a build that will trigger on the completion of the first, which will push the built image to our nexus docker repo.

| Parameter   | Required | Description                         |
| ----------- | -------- | ----------------------------------- |
| IMAGE_NAME  | Yes      | The name, i.e. hsa-openliberty-rest |
| GIT_URL     | Yes      | The git repo URL                    |
| GIT_CONTEXT | No       | A folder within the repo            |

Usage example:

```
oc process -f buildtemplate-docker.yaml IMAGE_NAME=hsa-openliberty-rest GIT_URL=https://bitbucket.org/ineraservices/hsa-docker.git GIT_CONTEXT=docker/hsa-openliberty-rest | oc create -f -
```

### Resources that are not covered with templates

Some resources are currently not covered by templates. They are instead created using the CLI.

#### S2I-build

To create the docker builds for our S2I-images, do NOT use the template for docker images above (it is only for the application docker images). Instead run the following command:

```
oc new-build https://bitbucket.org/ineraservices/hsa-docker.git --context-dir=docker/hsa-s2i-backend --name=hsa-s2i-backend
```

then start the build with

```
oc start-build hsa-s2i-backend
```

#### Storage

A storage is needed to store test reports. This storage is mounted to pods running tests as well as the Jenkins-image.


#### Routes

Routes are not created since it can be very individual for an OCP project how it wants to expose its services.
