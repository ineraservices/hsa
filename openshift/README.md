# HSA OpenShift

HSA utilizes two OCP-projects (Openshift Container Platform) in the test cluster:

###### dhsa (build environment)

The purpose of this project is to contain the CI-pipeline and everything necessary for it to work satisfactory. This includes for every component:

* Builds (to build war-artifacts and images)
* Deployments (to automatically deploy artifacts from the builds)
* Pods (for running tests)
* Volume storage (for test reports)

The deployments are fully controlled by the pipeline to be used by our automatic system tests and acceptance tests. A completed pipeline will result in an image being promoted to a "verified" image stream, that can be used by other environments.

###### thsa (test environment)

This project will only contain deployments of components that have completed a pipeline run in the build environment. It contains no builds, pipelines or automatic testing.

### Using Openshift

Openshift can be accessed through two ways:

###### The UI

The UI can be accessed at https://ind-ocpt1a-api.ocp.sth.basefarm.net/ It requires VPN as well as personal login credentials. This interfaces provides means of managing the project such as viewing running components, their logs, deploying and scaling.

###### The CLI

The CLI is provides similar capabilities as the UI. I can be used to bridge your local development environment to Openshift without having to navigate through a Web UI all the time which can make activities such as viewing logs, accessing pods and deploying easier, especially when used together with scripts.

To start using the CLI, copy a login command from the Web UI (top right corner) and paste into a terminal.

The CLI can be downloaded at https://github.com/openshift/origin/releases

Example: Port forwarding to LDAP-proxy in OCP:

```
oc port-forward hsa-ldap-proxy-7-7xqkm 1389:1389
```

You can now connect to LDAP proxy using localhost:1389.


### Structure of HSA in Openshift

Every unit in Openshift is referred to as a resource. To view them all, type:

```
oc types
```

It's a good idea to be familiarized with the different types of resources. HSA is structured using these resources:

* Build Configurations
    - Builds for our docker images
    - Builds for our sources (Vue-apps and war-artifacts)
    - Pipelines
* Deployment Configurations
    - Each component has one deployment configuration for every environment
* Services
    - A service fronts a Deployment and acts as a proxy and load balancer. It makes sure that requests are not lost when rolling out new deployments.
* Config maps
    - Configuration is stored here. Can be applied as a volume in a container, or as environment variables.
* Secrets (currently not used)
    - Sensitive configuration like passwords and certificates

###### Image streams

There are two kinds of image streams that contain runnable docker images of our services and applications. One with and one without the suffix "verified", i.e. "hsa-organization" and "hsa-organization-verified". When an image is built, it is always put into the first from which it is deployed by the pipeline to dhsa. If the tests pass, the image is also put into the second stream, which in turn is utilized by succeeding environments.

###### Access control of our environments

Currently access to our services is being restricted through whitelisting on routes. These are applied as annotations on the route, ex:

```
oc annotate routes <route> haproxy.router.openshift.io/ip_whitelist=< IPs/CIDRs >
```

### Jenkins

Jenkins is not installed using the catalog nor using a template. Instead it is based on the docker image "openshift/jenkins-2-centos7" found on docker hub. We have our own build located in the hsa-docker repo under docker/hsa-jenkins.

To add the jenkins deployment:
  - Make sure our custom image above is added and built in OpenShift
  - Add a 4Gb RWO-storage called "jenkins".
  - Apply the resource-yaml under resources for Jenkins.

Some things must be configured inside Jenkins:
  - Set the "linux" label on the master node.
  - If we still need it, add a windows node and label it with "windows" (and connect the slave)
  - Slack URL and Token needs to be configured in Manage Jenkins.

And the last step which is very hackish, but hopefully we won't need to do this often. Since Jenkins keeps track of the build numbers that we use in our versioning scheme (x.y.z.BUILDNUMBER) we need to restore these in case of a new deployment of Jenkins. This is done in the script console as such:

```
Jenkins.instance.getItem("dhsa").getItems()[n]
```

Where *n* is 0 -> until nothing is returned as a result. Every iteration, note the name of that item. Then - run this:

```
Jenkins.instance.getItem("dhsa").getItems()[n].updateNextBuildNumber(y)
```

Where *y* is the next build number that should be set. This can be found by finding the latest tag on the corresponding git repository.

