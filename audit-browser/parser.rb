require './utils.rb'
require './elastic.rb'

KEY_VALUE_DELIMITER = / +:/
ATTRIBUTE_COMPONENT = /^ +((Attr.*)|(\d{6})):/
ATTR_HEX = /^ +((Attr.*)|(\d{6})) ?:(.*)$/
REGULAR_ATTRIBUTES = /^\s+Attr Type\s*:(.*)\s+Attr Val\s+:(.*).*$/

def key_value_pairs(lines)
  lines.select { |line| line.match KEY_VALUE_DELIMITER }
    .select { |line| !line.match(ATTRIBUTE_COMPONENT) }
    .map { |line| line.strip.split KEY_VALUE_DELIMITER }
end

def attributes(lines)
  [["Attributes", hash(regular_attributes(lines) + hex_attributes(lines))]]
end

def regular_attributes(lines)
  lines.join("\n")
    .scan(REGULAR_ATTRIBUTES)
end

def hex_attributes(lines)
  []
end


t = nil
ARGV.take(20).each do |path|
  puts "> #{path}"
  t = File.open(path, 'r') do |file|
    file.slice_before(&:operation_line?) # Divide into operations
      .drop(1) # First record is garbage
      .map do |lines|
        key_value_pairs(lines) + attributes(lines)
      end
      .map { |a| hash(a) }
      .slice(0..-2)  # Drop last record which is a summary
      .let {|ops| Thread::new { send_to_elasticsearch(ops) } }
  end
end

t.join # Wait for last thread to finish
