# DO NOT CHECK IN OR SPREAD AUDIT LOGS

# Audit Browser

The Audit Browser is a ruby script that parses LDAP audit-logs and uploads them to ElasticSearch. They can then be browsed with Kibana.

First start ES & Kibana:

```
docker-compose up -d
```

When ES and Kibana are up and running (http://localhost:5601), run the script:

```
ruby parser.rb path/to/logs/**/*.dump
```

This will parse and upload the logs to ES under the index "operations"

In Kibana, create an index. Then import the _kibana_visualiations.json_ in _Management -> Saved Objects -> Import_ 
