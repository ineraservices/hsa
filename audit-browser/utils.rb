require 'date'

FROM_DATE_FORMAT = '%a %b %d %T.%N %Y'
TO_DATE_FORMAT = '%FT%T.%L'
OPERATIONS = /^[-]+ OPERATION \d+ [-]+$/


# Turns an array of arrays into a hash [[a,b], [c,d]] -> {a => b, c => d}
# Values of identical keys will be merged to an array
def hash(arrs)
  h = Hash.new
  arrs.each do |k,v|
    if h[k].class == NilClass
      h[k] = if k == "Req Attr #" then v else convert(v) end # Req Attr # is sometimes a number and sometimes a string
    elsif h[k].class == Array
      h[k].push(convert(v))
    else
      h[k] = [h[k], convert(v)]
    end
  end
  h
end

class Object
  def is_int?
    begin
      self.to_i.to_s == self.to_s
    rescue
      false
    end
  end

  def is_float?
    begin
      self.to_f.to_s == self.to_s
    rescue
      false
    end
  end

  def is_date?
    begin
      DateTime.strptime(self, FROM_DATE_FORMAT)
      return true
    rescue
      return false
    end
  end

  def let
    yield(self)
  end
end

class String
  def operation_line?
    self.match(OPERATIONS)
  end
end

# If possible, converts a string to int, float or date
def convert(value)
  if value.is_int? then value.to_i
  elsif value.is_float? then value.to_f
  elsif value.is_date? then DateTime.strptime(value, FROM_DATE_FORMAT).strftime(TO_DATE_FORMAT)
  elsif value.kind_of? String then value.force_encoding(::Encoding::UTF_8).gsub("\\c3\\a4", "ä") # Replace some stupid characters to ä, since it's present in "Läkare" everywhere.
  else
    value
  end
end
