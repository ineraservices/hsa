require 'json'
require 'net/http'

ELASTICSEARCH_URI = URI('http://localhost:9200/operations/operation/_bulk')

def format_bulk(item)
  # Elastic Search Bulk API format:
  # {"index": {"_id": "<unique_id>"}}
  # {<json_formated_operation_item>}
  "{\"index\": {\"_id\": \"#{item["OpUUID"] or item["ConnID"]}\"}}\n#{item.to_json}"
end

def send_to_elasticsearch(data)
  http = Net::HTTP.new(ELASTICSEARCH_URI.host, ELASTICSEARCH_URI.port)
  req = Net::HTTP::Post.new(ELASTICSEARCH_URI.request_uri, {'Content-Type' => 'application/x-ndjson'})
  d = data.map {|item| format_bulk item}
    .join("\n")
   
  req.body = d + "\n" # Bulk request must be terminated by newline ¯\_(ツ)_/¯
  response = http.request req
  if response.code == '200'
    puts "Uploaded #{data.size} operations"
  else
    puts response.body
  end
end
