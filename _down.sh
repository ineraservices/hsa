#!/bin/bash

cd "${0%/*}"
HSA_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." >/dev/null 2>&1 && pwd )"

echo "*** Stopping and removing codesystem-api"
cd "${HSA_ROOT}/hsa-codesystem/codesystem-war/"
docker-compose rm -v --stop --force

echo "*** Stopping and removing organization-api"
cd "${HSA_ROOT}/hsa-organization/organization/"
docker-compose rm -v --stop --force

echo "*** Stopping and removing ldap-proxy"
cd "${HSA_ROOT}/hsa-ldap-proxy/ldap-proxy/"
docker-compose rm -v --stop --force

echo "*** Stopping and removing graphql"
cd "${HSA_ROOT}/hsa-web/hsa-graphql/graphql-war/"
docker-compose rm -v --stop --force