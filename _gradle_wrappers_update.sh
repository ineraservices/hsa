#!/bin/bash

read -p "Do you know what you are doing? (Y/n)" -n 1 -read
echo
if [[ ! $REPLY =~ ^[Y]$ ]]; then
	echo "Take a look at the script first and understand what it does before you run it."
	exit 0
fi

# Sets working dir to project root and allow for spaces in path
cd "${0%/*}"
HSA_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." >/dev/null 2>&1 && pwd )"

# sets up gradle wrapper
shopt -s nocasematch
GRADLECMD="./gradlew"
case "`uname`" in
	darwin*)  GRADLECMD="./gradlew" ;;
	linux*)   GRADLECMD="./gradlew" ;;
	mingw*)   GRADLECMD="./gradlew" ;;
	msys*)    GRADLECMD="gradlew.bat" ;;
	*)        echo "unknown: `uname`" ;;
esac

paths=( 
	"${HSA_ROOT}/hsa-web/ui-search" 
	"${HSA_ROOT}/hsa-codesystem/"
	"${HSA_ROOT}/hsa-organization/"
	"${HSA_ROOT}/hsa/employee/"
	"${HSA_ROOT}/hsa-ldap-proxy/"
	"${HSA_ROOT}/hsa-web/hsa-graphql/"
)

# checks that no changes are uncommited to git
for path in "${paths[@]}"
do
	echo "------------------------------------------------"
	echo "checking git status in ${path}"
	cd ${path}
	if git diff-index --quiet HEAD --; then
    	echo "OK"
	else
		echo ""
		echo "${path}: This repo has changes, please fix first (commit or revert), and try again."
		exit 0
	fi
done

# does the update
NEW_VERSION="5.3.1"
for path in "${paths[@]}"
do
	echo "------------------------------------------------"
	echo "updating ${path}"
	cd ${path}
	${GRADLECMD} wrapper --gradle-version ${NEW_VERSION}
	${GRADLECMD} -v
done

echo "pushing changes to git"
for path in "${paths[@]}"
do
	echo "------------------------------------------------"
	echo "pushing ${path}"
	cd ${path}
	git add .
	git commit -m"updated gradle wrappers to ${NEW_VERSION}"
	# UNCOMMENT IF YOU KNOW WHAT YOU ARE DOING
	#git push
done

echo "All done!, don't forget to push!"

exit 0
