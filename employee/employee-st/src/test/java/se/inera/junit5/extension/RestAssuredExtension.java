package se.inera.junit5.extension;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.util.Optional;
import java.util.Properties;
import java.util.logging.Logger;

public class RestAssuredExtension implements BeforeAllCallback, AfterAllCallback {
    Properties props = new Properties();
    private static final Logger LOG = Logger.getLogger(RestAssuredExtension.class.getName());

    public RestAssuredExtension() {
        try {
            props.load(RestAssuredExtension.class.getResourceAsStream("test.properties"));
        } catch (Exception e) {
            LOG.warning("Unable to find test.properties, will try with default values.");
        }
    }

    @Override
    public void afterAll(ExtensionContext context) throws Exception {
        RestAssured.reset();
    }

    @Override
    public void beforeAll(ExtensionContext context) throws Exception {

        Optional<Integer> port = getProperty("port").map(Integer::valueOf);
        Optional<String> baseUri = getProperty("baseUri");
        Optional<String> basePath = getProperty("basePath");

        RestAssured.port = port.orElse(RestAssured.DEFAULT_PORT);
        RestAssured.baseURI = baseUri.orElse(RestAssured.DEFAULT_URI);
        RestAssured.basePath = basePath.orElse(RestAssured.DEFAULT_PATH);
        RestAssured.filters().size();

        RestAssured.requestSpecification = new RequestSpecBuilder()
                    .setContentType(ContentType.JSON)
                    .setAccept(ContentType.JSON)
                    .build();
    }

    private Optional<String> getProperty(String propertyName) {
        return Optional.ofNullable(props.getProperty(propertyName));
    }
}
