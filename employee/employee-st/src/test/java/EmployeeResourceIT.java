import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import se.inera.junit5.extension.RestAssuredExtension;

import static io.restassured.RestAssured.given;

@ExtendWith({RestAssuredExtension.class})
public class EmployeeResourceIT {

    @Test
    public void getNonExistent() {
        given().when()
                .get("nonexistent")
                .then()
                .statusCode(404);
    }
}
