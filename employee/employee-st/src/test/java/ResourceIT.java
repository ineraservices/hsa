import org.junit.jupiter.api.extension.ExtendWith;
import se.inera.junit5.extension.RestAssuredExtension;

@ExtendWith({RestAssuredExtension.class})
abstract class ResourceIT {

    static final String EMPLOYEE_PATH = "employee";
    static final String EXISTING_EMPLOYEE_ID = "3ebda4ad-79fc-4c43-ac0c-089d3ba93501";

    static final String NEW_EMPLOYEE_ID_1 = "ST_TEST_01";

    static final String NEW_EMPLOYEE_ID_2 = "ST_TEST_02";

    static final String NEW_EMPLOYEE_ID_3 = "ST_TEST_03";

    static final String NEW_EMPLOYEE_ID_4 = "ST_TEST_04";

    static final String NEW_EMPLOYEE_ID_5 = "ST_TEST_05";

}


