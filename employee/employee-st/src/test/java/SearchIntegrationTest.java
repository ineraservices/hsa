import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.msgpack.util.json.JSON;
import se.inera.employee.model.Employee;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SearchIntegrationTest extends ResourceIT {

    @Context
    UriInfo uriInfo;

    @Test
    @DisplayName("Should be able to find the newly created employee in the free-text search result. \n The result should not include employees which don't have the free-text in their searchable attributes.")
    void assertResultSuccessWhenFreeTextSearchingAfterAddingUnit() {

        Employee employee1Input = new Employee();
        employee1Input.title = "Mr.";
        employee1Input.hsaTitle = Arrays.asList("chef");
        employee1Input.fullName = "first1 full name1";
        employee1Input.givenName ="first1 given name1";
        employee1Input.nickName = "first1 nick name1";
        employee1Input.middleName = "first1 middle name1";
        employee1Input.postalAddress = "Svenssons gatan 120";
        employee1Input.hsaIdentity = NEW_EMPLOYEE_ID_1 + UUID.randomUUID();
        employee1Input.personalIdentityNumber = "191212121212";
        employee1Input.dn = "Country=SE,County=Stockholms county";

        //String unit2Input = String.format("{\"name\":\"really dc\",\"type\":\"Unit\",\"hsaIdentity\":\"%s\"}", NEW_HSA_ID_2);
        Employee employee2Input = new Employee();

        employee2Input.title = "Mr.";
        employee2Input.hsaTitle = Arrays.asList("chef");
        employee2Input.fullName = "second1 full name1";
        employee2Input.givenName ="second11 given name1";
        employee2Input.nickName = "second1 nick name1";
        employee2Input.middleName = "second1 middle name1";
        employee2Input.postalAddress = "Svenssons gatan 120";
        employee2Input.hsaIdentity = NEW_EMPLOYEE_ID_2 + UUID.randomUUID();
        employee2Input.personalIdentityNumber = "191212121213";
        employee2Input.dn = "Country=SE,County=Stockholms county";

        // LocalDateTime postingTime = LocalDateTime.now();
        LocalDateTime postingTime2 = LocalDateTime.from(ZonedDateTime.now(ZoneOffset.UTC)); // GMT+0

        // post first unit
        String put1Path = "http://localhost:9082/employee/resources/";
        String pathToFirstEmployee = given().body(employee1Input).when().post(put1Path)
                .thenReturn().getHeader("Location");

        // get first unit
        final Employee employee1 = given()
                .when().get(pathToFirstEmployee).as(Employee.class);


        // post second unit
        String put2Path = "http://localhost:9082/employee/resources/";
        String pathToSecondEmployee = given().body(employee2Input).when().post(put2Path).thenReturn().getHeader("Location");

        // get second unit
        final Employee employee2 = given()
                .when().get(pathToSecondEmployee).as(Employee.class);

        // check if the path to the units is available in the search result.
        // This is valid assertion test since the unique Id of the resource is included in the link.

        List<String> jsonResponse = given().when().get("http://127.0.0.1:9082/employee/resources/search/name1 first1?offset=0&size=200")
                .then().contentType(ContentType.JSON).extract().response()
                .getBody().jsonPath().get("content");

        assertTrue(jsonResponse.stream().anyMatch(jsoResp -> Arrays.stream(new String[]{employee1.hsaIdentity}).anyMatch(jsoResp::contains)));
        assertTrue(jsonResponse.stream().anyMatch(jsoResp -> Arrays.stream(new String[]{employee2.hsaIdentity}).anyMatch(jsoResp::contains)));


        // let's search for the word 'first'. The result should not include the second unit

        Response response2 = doGetRequest("http://127.0.0.1:9082/employee/resources/search/first1?offset=0&size=200");

        List<String> jsonResponse2 = response2.jsonPath().get("content");

        assertTrue(jsonResponse2.stream().anyMatch(jsoResp -> Arrays.stream(new String[]{employee1.hsaIdentity}).anyMatch(jsoResp::contains)));
        assertFalse(jsonResponse2.stream().anyMatch(jsoResp -> Arrays.stream(new String[]{employee2.hsaIdentity}).anyMatch(jsoResp::contains)));

        // remove units
        // TODO: 2018-10-25 use path in delete
        given().when().delete(  "http://127.0.0.1:9082/employee/resources/" + employee1.hsaIdentity)
                .then().statusCode(204);
        given().when().delete( "http://127.0.0.1:9082/employee/resources/" +employee2.hsaIdentity).then().statusCode(204);


    }

    public static Response doGetRequest(String endpoint) {
        RestAssured.defaultParser = Parser.JSON;

        return
                 given().headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
                        when().get(endpoint).
                        then().contentType(ContentType.JSON).extract().response();
    }

}
