# Link
all employees
    curl http://localhost:9082/employee/resources

mock test employee
    curl http://127.0.0.1:9082/employee/resources/mock

employee with certain hsaid
    curl http://127.0.0.1:9082/employee/resources/byHsaId/SE10101010-G5TZ

employee with a certain dn
   http://127.0.0.1:9082/employee/resources/byHsaId/SE10101010-G5TZ

employee by unique id
   http://127.0.0.1:9082/employee/resources/employee/Identity-101-4e71da7a-0734-4a90-ab69-ca83d42aa23e

delete by hsa id
    curl -X DELETE http://127.0.0.1:9082/employee/resources/byHsaId/SE10101010-G5TZ
