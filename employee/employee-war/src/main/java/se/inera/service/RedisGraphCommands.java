package se.inera.service;

import io.lettuce.core.dynamic.Commands;
import io.lettuce.core.dynamic.annotation.CommandNaming;

import java.util.List;

import static io.lettuce.core.dynamic.annotation.CommandNaming.Strategy.DOT;

/**
 * @author kdaham
 * Command interface to Redis Graph
 * @see https://oss.redislabs.com/redisgraph/commands/
 */
public interface RedisGraphCommands extends Commands {
    @CommandNaming(strategy = DOT)
    String graphExplain(String name, String query);

    @CommandNaming(strategy = DOT)
    List<Object> graphQuery(String name, String query);
}
