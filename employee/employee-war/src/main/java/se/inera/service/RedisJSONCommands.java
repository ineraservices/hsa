package se.inera.service;

import io.lettuce.core.dynamic.Commands;
import io.lettuce.core.dynamic.annotation.CommandNaming;

import static io.lettuce.core.dynamic.annotation.CommandNaming.Strategy.DOT;

/**
 * @author kdaham
 * Command interface to reJSON
 * @see http://rejson.io/commands/
 */
public interface RedisJSONCommands extends Commands {

    @CommandNaming(strategy = DOT)
    String jsonDel(String key, String path);

    @CommandNaming(strategy = DOT)
    String jsonGet(String key, String NOESCAPE, String path);

    @CommandNaming(strategy = DOT)
    String jsonSet(String key, String path, String json);

    @CommandNaming(strategy = DOT)
    String jsonMget(String key, String path);

    @CommandNaming(strategy = DOT)
    String jsonType(String key, String path);

    @CommandNaming(strategy = DOT)
    String jsonNumincrby(String key, String path, String number);

    @CommandNaming(strategy = DOT)
    String jsonNummultby(String key, String path, String number);

    @CommandNaming(strategy = DOT)
    String jsonStrappend(String key, String path, String jsonString);

    @CommandNaming(strategy = DOT)
    String jsonStrlen(String key, String path);

    @CommandNaming(strategy = DOT)
    String jsonArrappend(String key, String path, String json);

    @CommandNaming(strategy = DOT)
    String jsonArrindex(String key, String path, String jsonScalar);

    @CommandNaming(strategy = DOT)
    String jsonArrinsert(String key, String path, String index, String json);

    @CommandNaming(strategy = DOT)
    String jsonArrlen(String key, String path);

    @CommandNaming(strategy = DOT)
    String jsonArrpop(String key, String path);

    @CommandNaming(strategy = DOT)
    String jsonArrtrim(String key, String path, String start, String stop);

    @CommandNaming(strategy = DOT)
    String jsonObjkeys(String key, String path);

    @CommandNaming(strategy = DOT)
    String jsonObjlen(String key, String path);

    @CommandNaming(strategy = DOT)
    String jsonDebug(String subcommand);

    @CommandNaming(strategy = DOT)
    String jsonResp(String key, String path);
}