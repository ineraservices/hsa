package se.inera.employee.model;

import java.io.Serializable;
import java.util.List;

import javax.json.bind.JsonbBuilder;
import javax.json.bind.annotation.JsonbTypeDeserializer;


public class Employee implements Serializable {

    @JsonbTypeDeserializer(ListStringTypeDeserializer.class)
    public List<String> objectClass;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> cardNumber; // certattribut
	public String dn; // obligatoriskt m.a.p. persistence?
	public String cn; //obligatoriskt
	public String createTimeStamp;
	public String creatorsName;
	public String description;
	public String endDate;
	public String fullName; //obligatoriskt
	public String givenName;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> groupMembership;
	public String hospIdentityNumber;
	public String hsaAdminComment;
	public String hsaAltText;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> hsaDestinationIndicator;
	public String hsaEmigratedPerson;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> hsaGroupPrescriptionCode;
	public String hsaIdentity; //obligatoriskt
	public String hsaManagerCode;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> hsaMifareSerialNumber;
	public String hsaPassportBirthDate;
	public String hsaPassportNumber;
	public String hsaPassportValidThru;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> hsaPhoneticSearch;
	public String hsaProtectedPerson;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> hsaSimpleSearch;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> hsaSosNursePrescriptionRight;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> hsaSosTitleCodeSpeciality;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> hsaSystemRole;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> hsaSyncId;
	public String hsaSwitchboardNumber;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> hsaTextTelephoneNumber;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> hsaTelephoneNumber;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> hsaTitle;
	public String hsaVerifiedIdentityNumber;
	public String initials;
	public String jpegPhoto;
	public String mail;
	public String middleName; //obligatoriskt om finns 
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> mobile;
	public String modifiersName;
	public String modifyTimeStamp;
	public String nickName;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> occupationalCode;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> pager;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> paTitleName;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> paTitleCode;
	public String personalIdentityNumber; //obligatoriskt om finns (utlaendska personposter har passnummer istaellet)
	public String personalPrescriptionCode;
	public String postalAddress;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> seeAlso;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> serialNumber; //certattribut
	public String sn; //Obligatoriskt
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> specialityName;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> specialityCode;
	public String startDate;
	public String street;
	@JsonbTypeDeserializer(HoursDeserializer.class)
	public List<Hours> telephoneHours;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class) 
	public List<String> telephoneNumber;
	public String title;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> userCertificate; // certAttribut
	public String userPassword;
	public String userPrincipalName;
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> validNotBefore; // certAttribut
	@JsonbTypeDeserializer(ListStringTypeDeserializer.class)
	public List<String> validNotAfter; // certAttribut
    
    public Employee() {

    }

    public String getHsaIdentity() {
        return hsaIdentity;
    }

    public String toJson() {
        return JsonbBuilder.create().toJson(this);
    }
}
