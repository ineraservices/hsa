package se.inera.employee.model;

import javax.json.JsonString;
import javax.json.bind.serializer.DeserializationContext;
import javax.json.bind.serializer.JsonbDeserializer;
import javax.json.stream.JsonParser;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author kdaham
 */
public class ListStringTypeDeserializer implements JsonbDeserializer<List<String>> {
    @Override
    public List<String> deserialize(JsonParser parser, DeserializationContext ctx, Type rtType) {
        try {
            return ctx.deserialize(List.class, parser);
        } catch(Exception ex) {
            List<String> response = new ArrayList<>();
            response.add(parser.getValue().toString().replaceAll("^\"|\"$", ""));
//            response.add( ((JsonString)parser.getValue()).getString());
            return response;
        }
        //final JsonParser.Event event = ((JsonbParser) parser).getCurrentLevel().getLastEvent();
        /**
         if(event == JsonParser.Event.VALUE_STRING) {
         } else if(event == JsonParser.Event.START_ARRAY) {
         }
         */
    }
}
