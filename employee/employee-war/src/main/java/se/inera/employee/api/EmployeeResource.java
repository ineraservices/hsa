package se.inera.employee.api;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import se.inera.employee.model.Employee;
import se.inera.employee.service.EmployeeProvider;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static java.net.URLDecoder.decode;

@Path("")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
public class EmployeeResource {

    @Inject
    EmployeeProvider employeeProvider;

    @Context
    private UriInfo uriInfo;


    @GET
    public List<String> getEmployees() {
        return employeeProvider.getAllEmployees();
    }


    @GET
    @Path("/employee/{id}")
    public String getEmployee(@PathParam("id") String id) {
        return employeeProvider.getEmployeeById(id);
    }

    @GET
    @Path("/byHsaId/{hsaId}")
    public String getEmployeeByHsaID(@PathParam("hsaId") String hsaId) {
        return employeeProvider.getEmployeeByHsaId(hsaId);
    }


    @GET
    @Path("byDn/{dn}")
    public String getEmployeeByDn(@PathParam("dn") String dn) {
        return employeeProvider.getByDn(dn);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createEmployee(Employee employee) throws URISyntaxException {
      employeeProvider.persistEmployee(employee);
        return Response.created(new URI(uriInfo.getBaseUri()+"employee/"+employee.getHsaIdentity())).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response persistEmployee(Employee employee) throws URISyntaxException {
        employeeProvider.persistEmployee(employee);
        return Response.created(new URI(uriInfo.getBaseUri()+"employee/"+employee.getHsaIdentity())).build();
    }



    @GET
    @Path("mock")
    @Consumes(MediaType.APPLICATION_JSON)
    public String mockEmployee() {
        Employee employee = new Employee();

        Jsonb jsonb = JsonbBuilder.create();
        employee = jsonb.fromJson(Json.createReader(this.getClass().getResourceAsStream("/employeeJson.json")).readObject().toString(), Employee.class);

        employee.hsaIdentity = "Identity-101-" + UUID.randomUUID();
        employee.fullName = "Mocked fullname";
        employee.nickName = "Mocked nickName";
        employee.middleName = "mocked middle name";
        employee.personalIdentityNumber = "191212121212";
        employee.dn = "Country=SE,County=Stockholms county";
        employee.givenName ="mocked given name";
        employeeProvider.persistEmployee(employee);
        return employee.toJson();
    }

    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") String id) {
        employeeProvider.delete(id);
    }

    @DELETE
    @Path("/byHsaId/{hsaId}")
    public void deleteByHsaId(@PathParam("hsaId") String hsaId) {
        employeeProvider.deleteByHsaId(hsaId);
    }

    @DELETE
    @Path("byDn/{dn}")
    public void deleteEmployeeByDn(@PathParam("dn") String dn) {
        employeeProvider.deleteByDn(dn);
    }

}
