package se.inera.employee.service;

import io.lettuce.core.api.StatefulRedisConnection;
import se.inera.employee.model.Employee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A Class for looking up hsa ids.
 *
 * It will save indexes to Redis to be able so lookup hsa ids from other unique resources.
 */
class RedisKeyLookup {

    // The root for all keys in organization
    private static final String PREFIX_ROOT = "employee:id:";

    // Keys
    private static final String HSAID_KEY_PREFIX = PREFIX_ROOT + "hsaid:";

    // indices
    private static final String DN_TO_HSAID_INDEX_KEY_PREFIX = PREFIX_ROOT + "lookup:dn-to-hsaid:";
    private static final String HSAID_TO_DN_INDEX_KEY_PREFIX = PREFIX_ROOT + "lookup:hsaid-to-dn:";

    private StatefulRedisConnection<String, String> connection;

    RedisKeyLookup(StatefulRedisConnection<String, String> connection) {
        this.connection = connection;
    }

    void persistLookups(Employee employee) {
        persistDnLookup(employee.dn, employee.hsaIdentity);
    }

    private void persistDnLookup(String dn, String hsaId) {
        persist(
                DN_TO_HSAID_INDEX_KEY_PREFIX + cleanDn(dn),
                hsaId);
        setDnReverseLookup(hsaId, dn);
    }

    private void setDnReverseLookup(String hsaId, String dn) {
        persist(
                HSAID_TO_DN_INDEX_KEY_PREFIX + hsaId,
                cleanDn(dn));
    }

    private String cleanDn(String dn) {
        List<String> dirtyParts = Arrays.asList(dn.split(","));
        List<String> cleanParts = new ArrayList<>();
        for (String dirtyPart : dirtyParts) {
            cleanParts.add(dirtyPart.trim().toLowerCase());
        }
        return String.join(",", cleanParts);
    }

    String getHsaIdKeyFromDn(String dn) {
        String hsaid = get(DN_TO_HSAID_INDEX_KEY_PREFIX + cleanDn(dn));
        return getHsaIdKeyFromHsaId(hsaid);
    }

    String getParentHsaIdKeyFromDn(String dn) {
        return "organization:hsaid:" + get("organization:lookup:dn-to-hsaid:" + cleanDn(dn));
    }

    String getHsaIdKeyFromHsaId(String hsaId) {
        return HSAID_KEY_PREFIX + hsaId;
    }

    private String getHsaIdFromHsaIdKey(String hsaIdKey) {
        return hsaIdKey.replaceFirst(HSAID_KEY_PREFIX, "");
    }

    List<String> getAllHsaIdKeys() {
        return connection.sync().keys(getHsaIdKeyFromHsaId("*"));
    }
    List<String> getAllEmployeeIdKeys() {
        return connection.sync().keys(PREFIX_ROOT + "*");
    }

    private void persist(String key, String value) {
        connection.sync().set(key, value);
    }

    private String get(String key) {
        return connection.sync().get(key);
    }

    private void forget(String key) {
        connection.sync().del(key);
    }

    /**
     * clean up the lookups and remove the hsa id key
     * @param hsaId - the hsa id to remove
     */
    void forgetByHsaId(String hsaId) {
        String dn = get(HSAID_TO_DN_INDEX_KEY_PREFIX + hsaId);
        forget(DN_TO_HSAID_INDEX_KEY_PREFIX + dn);
        forget(HSAID_TO_DN_INDEX_KEY_PREFIX + hsaId);
        forget(getHsaIdKeyFromHsaId(hsaId));
    }

    void forgetByHsaIdKey(String hsaIdKey) {
        forgetByHsaId(getHsaIdFromHsaIdKey(hsaIdKey));
    }
}
