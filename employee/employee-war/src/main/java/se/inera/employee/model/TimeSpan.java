package se.inera.employee.model;

import java.io.Serializable;

/**
 * @author kdaham
 */
public class TimeSpan implements Serializable {
   public String fromDay;
   public String toDay;
   public String fromTime2;
   public String toTime2;
   public String comment;
   public String fromDate;
   public String toDate;
}
