package se.inera.employee.model;

import javax.json.JsonObject;
import javax.json.bind.serializer.DeserializationContext;
import javax.json.bind.serializer.JsonbDeserializer;
import javax.json.stream.JsonParser;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class HoursDeserializer implements JsonbDeserializer<List<Hours>> {

    private static final String INITIAL_ENDING_QUOTES = "^\"|\"$";

    @Override
    public List<Hours> deserialize(JsonParser parser, DeserializationContext ctx, Type rtType) {
        List<Hours> hours = new ArrayList<>();

        JsonParser.Event event = parser.next();
        if (event == JsonParser.Event.KEY_NAME) {
            parser.next();
            hours.add(populate(parser.getObject()));
        } else if (event == JsonParser.Event.START_OBJECT) {
            while (parser.hasNext() && (event != JsonParser.Event.END_OBJECT)){
                hours.add(populate(parser.getObject().getJsonObject("timeSpan")));
                parser.next();
            }
        }
        return hours;
    }

    private Hours populate(JsonObject obj) {
        Hours hours = new Hours();
        TimeSpan ts = new TimeSpan();
        ts.fromDay = obj.get("fromDay").toString().replaceAll(INITIAL_ENDING_QUOTES, "");
        ts.toDay = obj.get("toDay").toString().replaceAll(INITIAL_ENDING_QUOTES, "");
        ts.fromTime2 = obj.get("fromTime2").toString().replaceAll(INITIAL_ENDING_QUOTES, "");
        ts.toTime2 = obj.get("toTime2").toString().replaceAll(INITIAL_ENDING_QUOTES, "");

        if (obj.get("comment") != null)
            ts.comment = obj.get("comment").toString().replaceAll(INITIAL_ENDING_QUOTES, "");

        if (obj.get("fromDate") != null)
            ts.fromDate = obj.get("fromDate").toString().replaceAll(INITIAL_ENDING_QUOTES, "");

        if (obj.get("toDate") != null)
            ts.toDate = obj.asJsonObject().get("toDate").toString().replaceAll(INITIAL_ENDING_QUOTES, "");

        hours.timeSpan = ts;
        return hours;
    }
}
