package se.inera.employee.api.web;

import org.eclipse.microprofile.opentracing.Traced;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import se.inera.employee.service.EmployeeProvider;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.*;


/**
 * @author Tewelle
 */

@Stateless
@Produces(MediaType.APPLICATION_JSON)
@Path("")
@Traced
public class Search {

    @Inject
    EmployeeProvider employeeProvider;


    @Context
    private UriInfo uriInfo;

    @GET
    @Path("search/{match}")
    public String search(@DefaultValue("*") @PathParam("match") String match, @QueryParam("offset") Integer offset, @QueryParam("size") Integer size) {

        if(match.equals("*") || match.isEmpty()) return null;
        List<String> matches = new ArrayList<>();
        String[] searchWords = match.trim().split("[^\\w]+|[\\s]+|[\\-]+ | [',!,:,.,/,_]+ "); // split words by space and other non-word/special characters

        List<String> employees = employeeProvider.getAllEmployees();
        List<String> filteredOrganizations = new ArrayList<>();

        JSONParser jsonParser1 = new JSONParser();
        boolean elementHasMatch =false;
        for (String org : employees)
        {
            // weight shows the number of matches in an element and is used to sort the result array
            int weight = 0;

            JSONObject json = null;
            try {
                json = (JSONObject) jsonParser1.parse(org);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            for(String word: searchWords)
            {

                if(
                        (json.get("givenName")!=null && json.get("givenName").toString().toLowerCase().contains(word.toLowerCase())) ||
                            (json.get("hsaIdentity")!=null && json.get("hsaIdentity").toString().toLowerCase().contains(word.toLowerCase()) )  ||
                            (json.get("title")!=null && json.get("title").toString().toLowerCase().contains(word.toLowerCase()) )  ||
                            (json.get("middleName")!=null && json.get("middleName").toString().toLowerCase().contains(word.toLowerCase()) )  ||
                            ( json.get("hsaTitle")!=null && json.get("hsaTitle").toString().toLowerCase().contains(word.toLowerCase()) )   ||
                            ( json.get("postalAddress")!=null && json.get("postalAddress").toString().toLowerCase().contains(word.toLowerCase()))
                        )
                {
                    ++weight;
                    elementHasMatch = true;
                }
            }
            if (elementHasMatch) {
                json.put("weight", weight);
                filteredOrganizations.add(json.toJSONString());
            }
            elementHasMatch = false;

        }

        filteredOrganizations.stream().forEach(object ->{
            JSONParser jsonParser = new JSONParser();
            try {

                JSONObject json = (JSONObject) jsonParser.parse(object);

                JsonObjectBuilder builder = Json.createObjectBuilder();

                builder.add("type","Person");
                builder.add("name",json.get("title").toString().concat(" " + json.get("givenName").toString()) );
                builder.add("hsaTitle",json.get("hsaTitle").toString());
                builder.add("givenName",json.get("givenName").toString());
                builder.add("middleName",json.get("middleName").toString());
                // next line is temporary until we get the real path from the respective graph
                builder.add("path",json.get("dn").toString().replaceAll(",","/"));
                builder.add("link",uriInfo.getBaseUri() + "employees/"+json.get("hsaIdentity"));

                builder.add("weight",json.get("weight").toString());
                matches.add(builder.build().toString());
            }
            catch (ParseException e) {
                e.printStackTrace();
            }
        });

        List<JSONObject> jsonValues = new ArrayList<>();
        for (int i = 0; i < matches.size(); i++) {
            try {
                jsonValues.add((JSONObject) jsonParser1.parse(matches.get(i)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        sortArrayList(jsonValues);

        List<String> currentPage = new ArrayList<>();
        if (offset < (jsonValues.size()%size ==0 ? jsonValues.size()/size : jsonValues.size()/size + 1 )) {
            for (int i = offset * size; i < jsonValues.size() && (i - (offset * size)) < size; i++) {
                currentPage.add((jsonValues.get(i).toString()));

            }
        }

        // add meta data
        Map<String,Object> links = new HashMap<>();
        Map<String,Object> page = new HashMap<> ();
        links.put("first",uriInfo.getBaseUri()+"search/" + match +"?offset=0&size="+size);
        links.put("previous",uriInfo.getBaseUri()+"search/" + match +"?offset="+(offset -1) +"&size="+size);
        links.put("current",uriInfo.getBaseUri()+"search/" + match +"?offset="+offset+"&size="+ size);
        links.put("next",uriInfo.getBaseUri()+"search/" + match +"?offset="+(offset+1 )+"&size="+size);
        links.put("last",uriInfo.getBaseUri()+"search/" + match +"?offset="+(jsonValues.size()%size ==0 ? jsonValues.size()/size : jsonValues.size()/size + 1 )+"&size="+ size);
        page.put("size",size );
        page.put("totalElements",jsonValues.size() );
        page.put("totalPages",(jsonValues.size()%size ==0 ? jsonValues.size()/size : jsonValues.size()/size + 1 ) );
        page.put("currentOffset",offset );
        page.put("last",offset >= (jsonValues.size()%size == 0 ? jsonValues.size()/size : jsonValues.size()/size + 1) -1); // pages -1 = offset

        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("content",currentPage);
        responseBody.put("_links",links);
        responseBody.put("page",page);

        return new JSONObject(responseBody).toString();


    }

    public List<JSONObject> sortArrayList(List<JSONObject> jsonValues)
    {
        Collections.sort( jsonValues, new Comparator<JSONObject>() {
            //We can change "weight" with another field if we want to sort by that field
            private static final String KEY_NAME = "weight";

            @Override
            public int compare(JSONObject a, JSONObject b) {
                String valA = new String();
                String valB = new String();

                try {
                    valA = (String) a.get(KEY_NAME);
                    valB = (String) b.get(KEY_NAME);
                }
                catch (Exception e) {
                    //do something
                }

                return -valA.compareTo(valB);
                //if you want to change the sort order, simply use the following:
                //return -valA.compareTo(valB);
            }
        });
        return jsonValues;

    }

}