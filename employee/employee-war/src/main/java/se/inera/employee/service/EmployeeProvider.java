package se.inera.employee.service;

import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.dynamic.RedisCommandFactory;
import se.inera.employee.model.Employee;
import se.inera.service.RedisJSONCommands;
import se.inera.service.RedisGraphCommands;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

public class EmployeeProvider {

    private static final String REJSON_ROOT = ".";

    private static final String NOESCAPE = "NOESCAPE";

    private static final String GRAPH_NAME = "graph:country:se";
    private final static String ID_PREFIX = "employee:id:";
    private StatefulRedisConnection<String, String> connection;

    @Inject
    RedisClient client;

    RedisJSONCommands rejson;

    RedisGraphCommands regraph;

    private RedisKeyLookup keyLookup;

    @PostConstruct
    public void start() {
        try {
            StatefulRedisConnection<String, String> connection = client.connect();
            RedisCommandFactory factory = new RedisCommandFactory(connection);
            rejson = factory.getCommands(RedisJSONCommands.class);
            regraph = factory.getCommands(RedisGraphCommands.class);
            keyLookup = new RedisKeyLookup(connection);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Can't start EmployeeProvider");
        }
    }

    public void persistEmployee(Employee employee) {

        //addGraph(employee.dn, employee.hsaIdentity);
        //keyLookup.persistLookups(employee);
        // start transaction
        this.isOK(rejson.jsonSet(ID_PREFIX + employee.hsaIdentity, REJSON_ROOT, employee.toJson()));
        // end transaction

    }

    public String getEmployeeByHsaId(String hsaId) {
        return jsonGet(ID_PREFIX + hsaId);
    }

    public String getEmployeeById(String id) {
        return jsonGet(ID_PREFIX + id);
    }


    public List<String> getAllEmployees() {
        List<String> employees = new ArrayList<>();
        for (String hsaIdKey : keyLookup.getAllEmployeeIdKeys()) {
            employees.add(jsonGet(hsaIdKey));
        }
        return employees;
    }

    public String getByDn(String dn) {
        return jsonGet(keyLookup.getHsaIdKeyFromDn(dn));
    }

    public String getParentHsaIdByDn(String dn) {
        JsonObject parent = Json.createReader(new StringReader(jsonGet(keyLookup.getParentHsaIdKeyFromDn(dn)))).readObject();
        return parent.get("hsaIdentity").toString().trim().replace("\"","");
    }

    public void delete(String id)
    {
        client.connect().async().del(id);
    }

    public void deleteByHsaId(String hsaId) {
        keyLookup.forgetByHsaId(hsaId);
    }

    public void deleteByDn(String dn) {
        String hsaIdKey = keyLookup.getHsaIdKeyFromDn(dn);
        keyLookup.forgetByHsaIdKey(hsaIdKey);
    }

    private String jsonGet(String key) {
        return rejson.jsonGet(key, NOESCAPE, REJSON_ROOT);
    }

    private void isOK(String response) {
        if (!response.equals("OK")) {
            // throw new exception
        }
    }

    private void addGraph(String dn, String hsaIdentity) {
        List<String> dnList = asList(dn.split(","));
        List<String> parentDnList = dnList.subList(1, dnList.size());

        String leafiestParentDnBit = parentDnList.get(0).trim().substring(0, parentDnList.get(0).trim().indexOf("="));

        String parentObjectType;

        if (leafiestParentDnBit.equals("c")) {
            // throw new ExceptionOfSomeSort!!
            // a person should belong to some subordinate org, not country directly
            return;

        } else if (leafiestParentDnBit.equals("l")) {
            parentObjectType = "county";
        } else if (leafiestParentDnBit.equals("o")) {
            parentObjectType = "organization";
        } else {
            parentObjectType = "unit";
        }

        String parentDn = parentDnList.stream().map(Object::toString)
                .collect(Collectors.joining(","));

        String parentHsaId = getParentHsaIdByDn(parentDn);

        // assert parent node exist
        String nodeMatchQuery = "MATCH (a:" + parentObjectType + ") WHERE a.hsaid = '" + parentHsaId + "' RETURN a";

        List<Object> nodeSet;

        try {
            nodeSet = regraph.graphQuery(GRAPH_NAME, nodeMatchQuery);
        } catch (Exception e) {
            e.printStackTrace();
            nodeSet = null;
        }

        if (nodeSet == null || nodeSet.size() == 1) {
            // throw new ExceptionOfSomeSort!!
            // a person should belong to an already registered org entity
        } else {

            // create person and relation if not exists
            try {
                String createRelationQuery = "MATCH (a:" + parentObjectType + ") WHERE a.hsaid = '" + parentHsaId + "' " +
                        "CREATE (:person {hsaid:'" + hsaIdentity + "'})-[:childto]->(a)";
                regraph.graphQuery(GRAPH_NAME, createRelationQuery);
                // log any part of this creation?
                //LOG.info("Created resultSet" + resultSet);
            } catch (Exception e) {
                // do what?
            }
        }
    }
}
