package se.inera.redis.service;

import io.lettuce.core.RedisURI;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import static io.lettuce.core.RedisURI.Builder;


public class RedisExposer {

    @Inject @ConfigProperty(name="redis.host", defaultValue = "hsa-redis")
    String hostname;

    @Inject @ConfigProperty(name="redis.port", defaultValue = "6379")
    int port;

    @Produces
    public RedisURI expose() {
        return Builder.redis(hostname, port).build();
    }
}
