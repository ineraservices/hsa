#!/bin/bash

# Sets working dir to project root and allow for spaces in path
cd "${0%/*}"
HSA_ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")/.." >/dev/null 2>&1 && pwd)"

# Sets up parameters
# Full mode if parameter "-f" is set
# 	- Runs all tests
# 	- Checks versions
GRADLE_SKIP_TESTS=""
IS_FULL_MODE="TRUE"
IS_INIT_MODE="FALSE"
function echo_help() {
	echo "################# build script / documentation ##################"
	echo "Use this script as a guide to get your local environment up and"
	echo "running."
	echo " With no parameter script runs in normal mode."
	echo " Script parameters"
	echo "  -h : help"
	echo "       Things not working out as expected?"
	echo "       Maybe you lack access to docker images."
	echo "       You need to be logged in on docker.drift.inera.se in order to pull images, and"
	echo "       in order to log in to docker.drift.inera.se you need to have your VPN"
	echo "       connection to basefarm up and running."
	echo "       Terminal command for docker login: docker login docker.drift.inera.se"
	echo "       You will be prompted for username and password, which can be found in "
	echo "       openshift environment under /resources/secrets -> nexus-registry."
	echo "       see https://portal-test1.ind-ocp.sth.basefarm.net/console/project/dhsa/browse/secrets/nexus-registry"
	echo "  -a : Run everything (takes a long time, do this only when"
	echo "       updates to base images needed or first time)"
	echo "  -q : Run in quick mode"
	echo "      - Skips all unit-tests"
	echo "      - Skips docker pull"
	echo "      - Skips version checks"
	echo "##################################################################"
}

SECONDS=0
DATE_WITH_TIME=`date "+%Y%m%d-%H%M%S"`
echo Starting up... [${DATE_WITH_TIME}]

echo_help

case $1 in
-h)
	exit 0
	;;
-q)
	echo " >>> RUNNING IN QUICK MODE !!!"
	GRADLE_SKIP_TESTS=" -x test "
	IS_FULL_MODE=""
	;;
-a)
	echo " >>>  RUNNING EVERYTHING!!!"
	IS_INIT_MODE="TRUE"
	;;
*)
	echo " >>> RUNNING IN NORMAL MODE!!!"
	;;
esac

# sets up gradle wrapper
shopt -s nocasematch
GRADLECMD="./gradlew"
case "$(uname)" in
darwin*) GRADLECMD="./gradlew" ;;
linux*) GRADLECMD="./gradlew" ;;
mingw*) GRADLECMD="./gradlew" ;;
msys*) GRADLECMD="./gradlew.bat" ;;
*) echo "unknown: $(uname)" ;;
esac

### Checks that the correct java version is installed
if [ "$IS_FULL_MODE" == "TRUE" ] || [ "$IS_INIT_MODE" == "TRUE" ]; then
	if type -p java; then
		echo found java executable in PATH
		_java=java
	elif [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]]; then
		echo found java executable in JAVA_HOME
		_java="$JAVA_HOME/bin/java"
	else
		echo Java version should be 11!
	fi
	if [[ "$_java" ]]; then
		version=$("$_java" -version 2>&1 | sed -n ';s/.* version "\(.*\)\.\(.*\)\..*"/\1\2/p;')
		if [[ "$version" == 11* ]]; then
			echo "Java version is 11 aka OK!"
		elif [[ "$version" -eq 18 ]]; then
			echo "Java version is 1.8 which will soon be deprecated in favour of Java 11, please consider upgrade"
		else
			echo "Current Java version for this project is 11, please install."
			exit 0
		fi
	fi
fi

# pulls the latest images
if [ "$IS_FULL_MODE" == "TRUE" ] || [ "$IS_INIT_MODE" == "TRUE" ]; then
	docker pull docker.drift.inera.se/hsa/hsa-openliberty-rest
	if [ $? -eq 0 ]; then
		docker pull docker.drift.inera.se/hsa/hsa-redis
		docker pull docker.drift.inera.se/hsa/hsa-openliberty-ldap
	else
		echo "Setup script failed!  Is your VPN connected to bf-ssl-vpn.sth.basefarm.net/ssl?  You also need to log in to docker.drift.inera.se, see _up.sh -h for information."
		exit 0
	fi
fi

#echo "*** build ui-search and start container "
#cd ${HSA_ROOT}/hsa-web/ui-search
#npm install
#npm run build
#docker-compose up -d

# skipping this for now
#echo "*** build elasticsearch, fluentd, kibana and start containers"
#cd ${HSA_ROOT}/elk
#docker-compose up -d

echo "*** build code system api and start container"
cd "${HSA_ROOT}/hsa-codesystem/"
${GRADLECMD} ${GRADLE_SKIP_TESTS} clean deploy
cd "${HSA_ROOT}/hsa-codesystem/codesystem-war/"
docker-compose up -d

echo "*** build organization api and start containers"
cd "${HSA_ROOT}/hsa-organization/"
${GRADLECMD} ${GRADLE_SKIP_TESTS} clean deploy
cd "${HSA_ROOT}/hsa-organization/organization/"
docker-compose up -d

# turns off employee for the moment
#echo "*** build employee api and start container"
#cd "${HSA_ROOT}/hsa/employee/"
#cd "${HSA_ROOT}/hsa/employee/employee-war/"
#${GRADLECMD} ${GRADLE_SKIP_TESTS} clean deploy
#docker-compose up -d

echo "*** build ldap proxy and start container"
cd "${HSA_ROOT}/hsa-ldap-proxy/"
${GRADLECMD} ${GRADLE_SKIP_TESTS} clean deploy
cd "${HSA_ROOT}/hsa-ldap-proxy/ldap-proxy/"
docker-compose up -d

echo "*** build GraphQl and start container"
cd "${HSA_ROOT}/hsa-web/hsa-graphql/"
${GRADLECMD} ${GRADLE_SKIP_TESTS} clean deploy
cd "${HSA_ROOT}/hsa-web/hsa-graphql/graphql-war/"
docker-compose up -d

# echos running containers stats
docker ps

echo populate hsa-redis
cd "${HSA_ROOT}/hsa" && /bin/bash "${HSA_ROOT}/hsa/populate_redis.txt"
echo populate hsa-sqlserver
cd "${HSA_ROOT}/hsa" && /bin/bash "${HSA_ROOT}/hsa/populate_sqlserver.sh"


printf "Waiting for organization-api."
while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' http://localhost:9080/organization/resources/v0.3/countries/Sverige)" != "200" ]]; do
	sleep 2
	printf "."
done

printf "\n"
printf "\nALL DONE!\n"

if (( $SECONDS > 3600 )) ; then
    let "hours=SECONDS/3600"
    let "minutes=(SECONDS%3600)/60"
    let "seconds=(SECONDS%3600)%60"
    echo "Completed in $hours hour(s), $minutes minute(s) and $seconds second(s)" 
elif (( $SECONDS > 60 )) ; then
    let "minutes=(SECONDS%3600)/60"
    let "seconds=(SECONDS%3600)%60"
    echo "Completed in $minutes minute(s) and $seconds second(s)"
else
    echo "Completed in $SECONDS seconds"
fi

exit 0
