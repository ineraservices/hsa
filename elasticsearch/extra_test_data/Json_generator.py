import json
import string
import random

person = {
'first_name': "John",
"isAlive": True,
"age": 27,
"address": {
 "streetAddress": "21 2nd Street",
 "city": "New York",
 "state": "NY",
 "postalCode": "10021-3100"
 },
 "hasMortgage": None
}
"""Generate a random string of 15 characters length """
id = ''.join(random.choice(string.ascii_lowercase) for i in range(15))  
Index = {
'index': {'_index': 'simplesearch', '_type': 'text',  '_id': '3ebda4ad-79fc-4c43-ac0c-089d3ba93509991'}
words= {'words': 'PL2120001710-0001 Vastra Gotalands lan Skavde kommun',  'type': 'Unit'}

with open('person.json', 'w') as f:  # writing JSON object
	json.dump(person, f)
