# coding=utf-8
"""
Script for creating test data for simple search in HSA
The Faker package must be installed before running this script:
'pip install Faker'
"""
import csv
import faker
__author__ = 'Dennis Djenfer'
__date__ = '2019'
__credits__ = ['Dennis Djenfer, Inera']
__email__ = 'dennis.djenfer@inera.se'
# ========== Functions ========== #
def create_sql_init_data_from_redis_file():
    """  Reads the file "index.txt", that was created for Redis, and extracts data that can be inserted into a SQL Server
    table for testing the HSA simple search requirements. Expected table structure:
    create table search_table (        internal_entry_uuid nvarchar(100) not null primary key,        search_words nvarchar(1000) not null    );
    The extracted data are formated as an SQL INSERT statement and written to the file "search_db_init_from_file.sql":
     insert into search_table (internal_entry_uuid, search_words) values    ('3ebda4ad-79fc-4c43-ac0c-089d3ba93501', 'Sverige'),
     ...
     ('68eb5e0a-c850-45c0-8122-2db13e4cd7c5', 'PL2120001710-0001 Västra Götalands län Skövde kommun');
     Get "index.txt" from "https://bitbucket.org/ineraservices/hsa/src/master/data/organization/index.txt" and put it in    the same directory as the script.
        """
    # Prefix used for the uuid
    key1 = 'simpleSearch:index:id:'
    key1_len = len(key1)
    # String that precedes the search words
    key2 = 'words "'
    key3 = 'type "'
    key2_len = len(key2)
    key3_len = len(key3)
    with open('index.txt', 'r', encoding='utf-8') as fh_source, open('search_db_init_from_file.sql', 'w', encoding='utf-8') as fh_target:
        fh_target.write('insert into search_table (internal_entry_uuid, object_type, search_words) values\n')
        fl = fh_source.readlines()
        counter = 0
        # Read each line from the source file
        for sl in fl:
            ix = sl.find(key1)
            if ix > -1:
                # Current line contains a prefix for the uuid. Get the uuid
                ix2 = sl.find(' ', ix)
                uuid = sl[ix + key1_len:ix2]
                # print(uuid)
                # It is assumed that a line that contains a prefix for the uuid also contains search words
                ix3 = sl.find(key2, ix2)
                ix4 = sl.find('" ', ix3 + key2_len)
                words = sl[ix3 + key2_len:ix4]
                words2 = words.replace('\\', '')
                ix5 = sl.find(key3, ix2)
                ix6 = sl.find('"', ix5+key3_len)
                type = sl[ix5 + key3_len:ix6]
                type2 = type.replace('\\', '')
                # print(type)
                # print(words2)
                if counter > 0:
                     # Every line, except the last one, must end with a comma
                    fh_target.write(',\n')
                counter += 1
                fh_target.write("('{}', '{}', '{}')".format(uuid, type2, words2))
            if counter > 0:
                fh_target.write(';\n')
    return
def create_init_data_from_redis_file():
    """    Reads the file "index.txt", that was created for Redis, and extracts data that can be inserted into a table for  
       testing the HSA simple search requirements.  Get "index.txt" from "https://bitbucket.org/ineraservices/hsa/src/master/data/organization/index.txt" 
       and put it in    the same directory as the script.    The extracted data is returned as a list of tuples, where each tuple consist oftwo strings: 
          ('<uuid>', '<one or more words, seperated with space>') 
    """
    # Prefix used for the uuid
    key1 = 'simpleSearch:index:id:'
    key1_len = len(key1)
    # String that precedes the search words
    key2 = 'words "'
    key2_len = len(key2)
    key3 = 'type "'
    key3_len = len(key3)
    data_list = []
    with open('index.txt', 'r', encoding='utf-8') as fh_source:
        fl = fh_source.readlines()
        # Read each line from the source file
        for sl in fl:
            ix = sl.find(key1)
            if ix > -1:
                # Current line contains a prefix for the uuid. Get the uuid
                ix2 = sl.find(' ', ix)
                uuid = sl[ix + key1_len:ix2]
                # It is assumed that a line that contains a prefix for the uuid also contains search words
                ix3 = sl.find(key2, ix2)
                ix4 = sl.find('" ', ix3 + key2_len)
                words = sl[ix3 + key2_len:ix4]
                words2 = words.replace('\\', '')
                ix5 = sl.find(key3, ix2)
                ix6 = sl.find('"', ix5+key3_len)
                type = sl[ix5 + key3_len:ix6]
                type2 = type.replace('\\', '')
                t = (uuid, type, words2)
                data_list.append(t)
    return data_list
def create_random_sql_init_data(data_list_from_file, max_random_data=200):
    """    Creates a csv file named "search_db_init_fake.csv" with generated values that can be loaded into a SQL Server table     with the BULK INSERT statement.
    The purpose is to test the HSA simple search requirements. Expected table structure:
    create table search_table (        internal_entry_uuid nvarchar(100) not null primary key,        search_words nvarchar(1000) not null    );
     The "data_list_from_file" parameter contains a list of tuples that will be usedalongside the generated fake data.
     The generated csv file contains two columns and no header:    - uuid    - one or more words, seperated with space
     Note! SQL Server does not support insert statements with more than 1000 rows. Use BULK INSERT instead:
     https://dba.stackexchange.com/questions/82921/the-number-of-row-value-expressions-in-the-insert-statement-exceeds-the-maximum
     https://docs.microsoft.com/en-us/sql/relational-databases/import-export/bulk-import-and-export-of-data-sql-server?view=sql-server-ver15
     https://codingsight.com/sql-server-bulk-insert-part-1/    "...BULK INSERT statement is much faster and robust than using other methodologies"
     About 7 min to create 1 000 000 rows    About 2 min to BULK INSERT into SQL Server
     About 12 sek to execute a LIKE query    :param data_list_from_file: (list) A list with tuples that consist of two strings:
           ('<uuid>', '<one or more words, seperated with space>')    :param max_random_data: (int) The number of rows created from fake data
             :return: Nothing
    """
    # Use "newline=''" to get rid of extra new line characters at the end of each line
    #  Encoding with 'utf-8' does not work with swedish characters when the file is moved from a Windows host to a Linux
    #  Docker-container and read into SQL Server with BULK INSERT. Encoding 'utf-16'does the job.
    #  Use BULK INSERT with datafiletype = 'widechar'
    with open('search_db_init_fake.csv', 'w', newline='', encoding='utf-16') as fh_target:
        w_csv = csv.writer(fh_target, delimiter=';')
        #  Use the Faker package to generate fake data. Create a localized faker
        fake = faker.Faker('sv_SE')
        #  Add extra providers
        fake.add_provider(faker.providers.address)
        #  Calculate where data from "data_list_from_file" shall be inserted among the rows with the generated fake data
        len_data_from_file = len(data_list_from_file)
        if max_random_data < len_data_from_file:
            max_random_data = len_data_from_file
    k = max_random_data // len_data_from_file  # Count how many items from "data_list_from_file" that has been written
    j = 0
    for i in range(max_random_data):
        uuid = fake.uuid4()
        # 21 characters that simulates a HSA-id
        hsaid = fake.iban()
        # From address provider
        county = fake.state()
        company = fake.company()
        city = fake.city()
        random_words = fake.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None)[:-1]
        name = fake.name()
        ssn = fake.ssn()
        # Create the string with words, delimited by space, used by simple search
        words = ' '.join((hsaid, county, company, city, random_words, name, ssn))
        # Insert data from "data_list_from_file" each k row
        if i % k == 0:
            # print('random: {}, file: {}'.format(i, j))
            if j < len_data_from_file:
                t = data_list_from_file[j]
                w_csv.writerow(t)
                j += 1
                type = t[1]
                w_csv.writerow([uuid, type, words])
    return
def create_random_es_init_data(data_list_from_file, max_random_data = 200, no_of_files = 1):
    """    Creates a newline delimited JSON (NDJSON) with generated values that can be posted to Elasticsearch bulk API to
    create an index from the supplied data (https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-bulk.html).
    The Bulk API has an upper limit for the supplied data. 1 miljon documents didn't work, but a file with 50 000
     documents was OK. Use the 'no_of_files' parameter to decide how many files the data should be divided into.
     The "data_list_from_file" parameter contains a list of tuples that will be usedalongside the generated fake data.
      The generated csv file contains two columns and no header:    - uuid    - one or more words, seperated with space
      About 9 min to create 1 000 000 documents in 5 files    About ? min to BULK INSERT into Elasticsearch
      About 2.8 sek to execute a query with two words with wildcards both in front and at the end of each word.
      a second run of the same query took 0.5 sek.  a third run, but with a slightly modified query took 0.5 sek
    :param
    data_list_from_file: (list)
    A list with tuples that consist of two strings:        ('<uuid>', '<one or more words, seperated with space>'):param max_random_data: (int)
    The number of rows created from fake data
    :param no_of_files: (int) The total number of files to create
    :return: Nothing
    """
    file_name_base = 'search_db_init_fake'
    suffix = '.json'
    # Use the Faker package to generate fake data. Create a localized faker
    fake = faker.Faker('sv_SE')
    # Add extra providers
    fake.add_provider(faker.providers.address)
    # Calculate where data from "data_list_from_file" shall be inserted among the rows with the generated fake data
    len_data_from_file = len(data_list_from_file)
    if max_random_data < len_data_from_file:
        max_random_data = len_data_from_file
        k = max_random_data // len_data_from_file
        i = 0
        # Count how many items from "data_list_from_file" that has been written
        j = 0
        # Calculate how many rows that should be written into each file
        rows_per_file = max_random_data // no_of_files
        for file_no in range(no_of_files):
            file_name = file_name_base + str(file_no) + suffix
            if file_no < (no_of_files - 1):
                r = rows_per_file
            else:
                r = max_random_data - file_no * rows_per_file
                # encoding='utf-8' must be used for the json file to be accepted by Elasticsearch
                #  The json file uses newline='\n' (Unix) to prevent the end of line character from being translated to '\r\n' (Windows)
            with open(file_name, 'w', newline='\n', encoding='utf-8') as fh_es:
                for fi in range(r):
                    i += 1
                    uuid = fake.uuid4()
                    # 21 characters that simulates a HSA-id
                    hsaid = fake.iban()
                    # From address provider
                    county = fake.state()
                    company = fake.company()
                    city = fake.city()
                    random_words = fake.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None)[:-1]
                    name = fake.name()
                    ssn = fake.ssn()
                    # Create the string with words, delimited by space, used by simple search
                    words = ' '.join((hsaid, county, company, city, random_words, name,ssn))
                    type =''
                    # Insert data from "data_list_from_file" each k row
                    if i % k == 0:
                        # print('random: {}, file: {}'.format(i, j))
                        if j < len_data_from_file:
                            t = data_list_from_file[j]
                            type = t[1],
                            # Write to ndjson file
                    fh_es.write('{{"index":{{"_id":"{}"}}}}\n'.format(t[0]))
                    fh_es.write('{{"internal_entry_uuid":"{}","type":"{}","search_words":"{}"}}\n'.format(t[0],t[1], t[2]))
                    j += 1
                    # Write to ndjson file
                    fh_es.write('{{"index":{{"_id":"{}"}}}}\n'.format(uuid))
                    fh_es.write('{{"internal_entry_uuid":"{}","type":"{}","search_words":"{}"}}\n'.format(uuid, type, words))
    return
# ========== Main program ========== #
if __name__== "__main__":
    data_list = create_init_data_from_redis_file()
    # create_random_sql_init_data(data_list, 1000000)
    create_random_es_init_data(data_list, 45, 5)
    create_sql_init_data_from_redis_file()

