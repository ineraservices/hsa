insert into search_table (internal_entry_uuid, object_type, search_words) values
('3ebda4ad-79fc-4c43-ac0c-089d3ba93501', 'Country', 'Sverige');
,
('71367294-f2fd-4544-873e-6fc591e1bd36', 'County', 'Stockholms län Sverige');
,
('30582572-8dc3-44c8-9721-90a9177033d4', 'County', 'Uppsala län Sverige');
,
('7b7ac165-7854-4c4c-9fd3-addc6fccddaf', 'County', 'Sörmlands län Sverige');
,
('6307e277-1d04-4c6b-8afb-f32836267203', 'County', 'Östergötlands län Sverige');
,
('742f9f5a-283c-438b-b8ec-6a5cbd29ac73', 'County', 'Jönköpings län');
,
('1b367aa0-9add-4b25-a8db-751d22db11c8', 'County', 'Kronobergs län');
,
('5e9a2ce2-2fe7-4fe8-afd5-fe3753c9c5fb', 'County', 'Kalmar län');
,
('4edc7e0a-b137-4775-b2b9-7d136cb31b2a', 'County', 'Gotlands län');
,
('a0723bde-7929-4ed3-aff4-d162bdfa4dfd', 'County', 'Blekinge län');
,
('29d9b0e2-40e5-4239-bd0b-ee3c3b21d099', 'County', 'Skåne län');
,
('c95ef655-f53d-4604-8f6f-6cdb72deb3a5', 'County', 'Hallands län');
,
('24ab6fc6-23ae-450e-b416-29233c033c1c', 'County', 'Västra Götalands län');
,
('35249932-a24e-48b7-a11a-0080b18dad01', 'County', 'Värmlands län');
,
('09fee888-b4c2-42bd-98c1-587ee13b1021', 'County', 'Örebro län');
,
('87e6b64f-ec35-49ce-9c4b-5ce0dd600884', 'County', 'Västmanlands län');
,
('448862e2-230d-4db1-9dd3-fd98ff27fe33', 'County', 'Dalarnas län');
,
('1bb184fc-2ad6-4be5-9c54-f04018d6c547', 'County', 'Gävleborgs län');
,
('14091e24-a30d-4bc0-b43e-c331839f5acb', 'County', 'Västernorrlands län');
,
('027ef783-a300-4ad3-807a-aa28ba876174', 'County', 'Jämtlands län');
,
('c874e7e5-090b-4287-8987-782ea1a9a564', 'County', 'Västerbottens län');
,
('d50a9c5c-880d-451d-b7db-2e0864332cee', 'County', 'Norrbottens län');
,
('2bf47d16-f3cf-4edd-bec0-efa113b5650c', 'County', 'Öster län');
,
('5ce91947-94ec-481c-bdd8-bda67414d65e', 'Organization', 'SE-01-01 Stockholms läns landsting');
,
('2c2ca7fd-26db-4fe9-a12c-9170436ba5f2', 'Organization', 'SE-20-01 Landstinget Dalarna');
,
('8eab6074-2c06-43ab-b096-817bde3b185c', 'Organization', 'SE-17-01 Värmlands län Landstinget i Värmland');
,
('3aaf2fdd-3951-455e-afca-d26fca790207', 'Organization', 'SE-25-01 Norrbottens län Tussilagon');
,
('1bdc0925-33c0-489c-877e-74e0ad39ea59', 'Organization', 'SE-25-02 Norrbottens län Snödroppen');
,
('5ad14783-4361-4619-85f1-a0bc27fb9938', 'Organization', 'GOT1234LAND Region Gotland');
,
('1df57352-8c03-4e38-adc0-c3e2a3ff0e32', 'Organization', 'TSTNMT2321000156-0001 Värmlands län Nordic MedTest');
,
('0492e34a-1cb3-4a76-8082-fc98a5e71658', 'Function', 'TSTNMT2321003456-1003 Värmlands län Nordic MedTest Nordic Function');
,
('92b1f660-de03-40b1-afcc-74b59f75efc2', 'Unit', 'TSTNMT2321000156-0002 Värmlands län Nordic MedTest Nordic Unit');
,
('afad03a9-e7f0-44b4-9913-33036751ab44', 'Function', 'TSTNMT2321003456-1001 Värmlands län Nordic MedTest Nordic Unit Function');
,
('7a73d615-2ffa-45f4-9092-af7c88b73c5d', 'Unit', 'TSTNMT2324252627-0001 Värmlands län Nordic MedTest enhet utan underordnade objekt');
,
('70a7346a-f331-49cc-825c-52e2f337a387', 'Unit', 'TEST999999994-0001 Värmlands län Nordic MedTest enhet med underordnade objekt');
,
('95d0c924-8062-4ec2-b470-09bbc07f9e1e', 'Unit', 'TEST999999994-0011 Värmlands län Nordic MedTest enhet med underordnade objekt underenhet');
,
('dd699457-9a18-409b-8252-5ab0428485de', 'Organization', 'TSTNMT2321003456-0001 Svensk Organisation');
,
('82153d75-719a-40a3-bd1b-3818b99b2c01', 'Unit', 'TSTNMT2321003456-0002 Svensk Organisation Svensk enhet');
,
('2a674d48-2579-4592-9b36-48a4ced6cde4', 'Function', 'TSTNMT2321003456-1000 Svensk Organisation Svensk enhet Svensk funktion ');
,
('9dd1a2eb-d4cb-44de-92ce-ef9723241ca6', 'Function', 'TSTNMT2321003456-1002 Värmlands län Nordic MedTest enhet med underordnade objekt underenhet funktion');
,
('6a396087-e2cc-4953-b2b1-fffa42ba0f2e', 'Unit', 'TSTNMT232100156-0001 Värmlands län Nordic MedTest Nordic Unit4 Hemvägen Karlstad');
,
('8cbeb8cc-9ef4-42d3-9223-b8201f022b23', 'Organization', 'TSTNCO1919191919-0001 Värmlands län Nordic Organization Maximum');
,
('d4b1a084-0b30-429b-acb9-8feaee4f93db', 'Employee', 'EMP-1000000001 Värmlands län Nordic Organization Maximum Anna Fia Inera 19810101-0011');
,
('d4b1a084-0b30-429b-acb9-8feaee4f93da', 'Employee', 'EMP-1000000002 Värmlands län Nordic Organization Maximum Censur X Sekretessblom 19810101-0012');
,
('58b3ddbf-db81-4ea5-b678-0933eca47701', 'Employee', 'EMP-2000000002 Svensk Organisation Sven O Person 19820202-0022');
,
('d21eb603-af32-46b5-a3b3-bceb4ce2755e', 'Employee', 'EMP-3000000003 Svensk Organisation Svensk enhet Sven Ou Person 19830303-0033');
,
('877f0b8b-9848-4f69-a667-31cb4436eda3', 'Organization', 'TSTBLT2321000156-0001 Blekinge län Region Blekinge Synk');
,
('68eb5e0a-c850-45c0-8122-2db13e4cd7c5', 'Organization', 'PL2120001710-0001 Västra Götalands län Skövde kommun');
;
;
;
