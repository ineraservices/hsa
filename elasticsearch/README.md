# Introduction 
This folder includes simple search related Elasticsearch configuration scripts and initial test data.
A detailed documentation and set of instructions is available on HSA confluence här: https://inera.atlassian.net/wiki/spaces/HSANG/pages/73597050/Elasticsearch+index+design+-del+2

# Installation Elasticsearch 
A docker-compose YML file is used to start a local Elasticsearch instance which listens on port 9200 unless changed in the file.
Navigate to the place where it is located, the same place as this README.md if not changed, and run:

    docker-compose up -d  
Then, check if the instance is up and ready for request using:
 
    CURL http://localhost:9200
   
If it is ok, a response of the following type should come:

    {
      "name" : "simplesearch-Node01",
      "cluster_name" : "simplesearch-Node-docker-cluster",
      "cluster_uuid" : "PabhaSH-S06-R_7L5bPVrw",
      "version" : {
        "number" : "7.4.2",
        "build_flavor" : "default",
        "build_type" : "docker",
        "build_hash" : "2f90bbf7b93631e52bafb59b3b049cb44ec25e96",
        "build_date" : "2019-10-28T20:40:44.881551Z",
        "build_snapshot" : false,
        "lucene_version" : "8.2.0",
        "minimum_wire_compatibility_version" : "6.8.0",
        "minimum_index_compatibility_version" : "6.0.0-beta1"
      },
      "tagline" : "You Know, for Search"
    } 
# Install plugins
Plugins can be installed either at Elasticsearch docker-build time or after the docker installs is installed and running.
This is explained more in the confluence page linked above. 
There is also a shell script which can do the installation after docker run: 

    ./install-plugins.sh
# Creating an index
 To create the index, we pass some parameters which specify the data definitions and structure of the index fields as well as other filters like language analysis.
 This is also described in the confluence documentation. 
 The definition of the parameters is also saved in a file, index_settings.json which can be passed as input while creating the index.
 Moreover, to support phonetic search based on a given list of homophones, we need to copy that list to the ..configure/ folder of the running docker container.
 Using the ADD option in a Dockerfile could have been better if we had built our image in that way.
 For this time, let's copy our swedish_homophones.txt list into the elasticsearch instances.
 We have three nodes defined in our docker-compose file. 
 
     docker cp index_settings.json elasticsearchNode01:/usr/share/elasticsearch/config/swedish_homophones.txt
     docker cp index_settings.json elasticsearchNode02:/usr/share/elasticsearch/config/swedish_homophones.txt
     docker cp index_settings.json elasticsearchNode02:/usr/share/elasticsearch/config/swedish_homophones.txt
     
 A simple CURL command to create the index which also takes the homophones list looks like:  
 
    curl  -s  -XPUT -H 'Content-Type: application/json' 'http://localhost:9200/simplesearch' --data-binary @index_settings.json 
# Load initial documents (index)
The initial redis-based index data is available in the redis_index.txt file. 
The redis index data is converted into another JSON structure which can be used as a bulk input while creating a document in elasticsearch.
The converted data is found in 'initial_index_data.json' 
To insert these documents into our Elasticsearch index, simplesearch in this case:

    curl -s -H "Content-Type: application/json" -XPOST "localhost:9200/simplesearch/_bulk" --data-binary "@initial_index_data.json"       
This command is also saved in a file which you can run as:

    ./load_initial_data.sh
# Search document
Having loaded the initial data above, we can run different types of search queries.
When we created the index, we had defined two types of analyzers, one with phonetic support and another with without.

    swedish_analyzer - assumes phonetic search while
    swedish_analyzer_exact_match - doesn't consider phonetic search 
Thus we need to choose which analyzer we wanted to use at search-time. 
# Search with phonetic support  
Doing a simple GET request to http://localhost:9200/simplesearch/_search with the following body will search for documents containing 'stockholm'  

    {
        "query": {
        	"bool": {
          "must": [{
          	"match":{
            "search_words": {
            "query": "bäck damen", // kommer hitta även beck
            "operator": "OR",
            "analyzer": "swedish_analyzer"
            }}
            }]
        	}
        }
    }

# Search without phonetic support 
This will be used for our exact match searching and the request body looks like:

    {
        "query": {
        	"bool": {
          "must": [{
          	"match":{
            "search_words": {
            "query": "beck damen",
            "operator": "AND",
            "analyzer": "swedish_analyzer_exact_match"
            }}
            }]
        	}
        }
    }
# Generate test data for extra testing
Navigate the the extra_test_data folder if you want to play with generating extra test data for testing.
# 