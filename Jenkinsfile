pipeline {
  agent any
    stages {
      stage ('Bitbucket checkout') {
        steps {
          git 'https://bitbucket.org/ineraservices/hsa.git'
        }
      }
      stage ('Docker build base images') {
        steps {
          sh "cd docker && ./build.sh"
        }
      }
      stage ('Redis-Graph build jar') {
        steps {
          sh "cd redis-graph && ./gradlew build test install"
        }
        post {
          always {
            junit 'redis-graph/build/test-results/test/*.xml'
          }
        } 
      }
      stage ('Codesystem build war and image') {
        steps {
          sh "cd codesystem && ./gradlew build"
          sh "cd codesystem && ./gradlew docker"
        }
      }
      stage ('Codesytem integration tests') {
        steps {
          catchError {
          sh "cd codesystem && ./gradlew st -PDOCKER_COMPOSE"
          }
        }
        post {
          always {
            junit 'codesystem/codesystem-st/build/test-results/st/*.xml'
            script {
              currentBuild.result = "SUCCESS"
            }  
          }
        } 
      }
      stage ('Employee build war and image') {
        steps {
          sh "cd employee && ./gradlew build"
          sh "cd employee && ./gradlew docker"
        } 
      }
      stage ('Employee integration tests') {
        steps {
          catchError {
            sh "cd employee && ./gradlew st -PDOCKER_COMPOSE"
          }
        }
        post {
          always {
            junit 'employee/employee-st/build/test-results/st/*.xml'
            script {
              currentBuild.result = "SUCCESS"
            }  
          }
        } 
      }
      stage ('Organization build war and image') {
        steps {
          sh "cd organization && ./gradlew build"
          sh "cd organization && ./gradlew docker"
        }
      }
      stage ('Organization integration tests') {
        steps {
          catchError {
            sh "cd organization && ./gradlew st -PDOCKER_COMPOSE"
          }
        }
        post {
          always {
            junit 'organization/organization-st/build/test-results/st/*.xml'
            script {
              currentBuild.result = "SUCCESS"
            }  
          }
        } 
      }
      stage ('LdapProxy build war and image') {
        steps {
          catchError {
            sh "cd ldap-proxy && ./gradlew build"
            sh "cd ldap-proxy && ./gradlew docker"
          }
        }
        post {
          always {
            junit 'ldap-proxy/ldap-proxy/build/test-results/test/*.xml'
            script {
              currentBuild.result = "SUCCESS"
            }  
          }
        } 
      }
      stage ('LdapProxy integration tests') {
        steps {
          catchError {
            sh "cd ldap-proxy && ./gradlew st -PDOCKER_COMPOSE"
          }
          catchError {
            sh "cd ldap-proxy && ./gradlew cucumber -PDOCKER_COMPOSE"
          }
        }
        post {
          always {
            junit 'ldap-proxy/ldap-proxy-st/build/test-results/st/*.xml'
            cucumber fileIncludePattern: '**/cucumber.json', reducingMethod: 'NONE', sortingMethod: 'ALPHABETICAL'
            script {
              currentBuild.result = "SUCCESS"
            }  
          }
        } 
      }
      stage ('UI Search build') {
        steps {
          sh "gradle build"
          sh "gradle test"
          sh "gradle docker"
        }

      }
      stage ('Build Kubernetes Helm artifact') {
        steps {
          sh "helm package ."
        }
      }
      stage('Deploy') {
        steps {
          sh 'helm install' 
        }
      }
    }
}